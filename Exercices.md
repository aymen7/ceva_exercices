# NodeJs

## Exercice: Is there a problem? _(1 points)_

```javascript
// Call web service and return count user, (got is library to call url)
async function getCountUsers() {
  return { total: await got.get('https://my-webservice.moveecar.com/users/count') };
}

// Add total from service with 20
async function computeResult() {
  const result = getCountUsers();
  return result.total + 20;
}
```

### Response

1. Incorporated the `await` keyword into the getCountUsers invocation within the `computeResult` function, ensuring proper handling of the `asynchronous` behavior of the function.
2. Make sure to handle errors appropriately, such as using `try-catch` blocks or `.catch()` when calling these asynchronous functions.

## Exercice: Is there a problem? _(2 points)_

```javascript
// Call web service and return total vehicles, (got is library to call url)
async function getTotalVehicles() {
    return await got.get('https://my-webservice.moveecar.com/vehicles/total');
}

function getPlurial() {
    let total;
    getTotalVehicles().then(r => total = r);
    if (total <= 0) {
        return 'none';
    }
    if (total <= 10) {
        return 'few';
    }
    return 'many';
}
```

### Response

1. Wrapped the logic in a `try-catch` block to handle errors appropriately.
2. pass the `if()` blocks inside the callback for the `.then()` to make sure we got the value of `total` or it will always return `many` or we can use `await` to correctly handle the `asynchronous` call to `getTotalVehicles`.
3. This ensures that `getPlural` waits for `getTotalVehicles` to complete before checking the value of total.

## Exercice: Unit test _(2 points)_

Write unit tests in jest for the function below in typescript

```typescript
import { expect, test } from '@jest/globals';

function getCapitalizeFirstWord(name: string): string {
  if (name == null) {
    throw new Error('Failed to capitalize first word with null');
  }
  if (!name) {
    return name;
  }
  return name.split(' ').map(
    n => n.length > 1 ? (n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()) : n
  ).join(' ');
}

test('should capitalize first word of a string', () => {
  // Test case 1: Basic string
  expect(getCapitalizeFirstWord('hello world')).toBe('Hello World');

  // Test case 2: Empty string
  expect(getCapitalizeFirstWord('')).toBe('');

  // Test case 3: Null input
  expect(() => getCapitalizeFirstWord(null)).toThrowError('Failed to capitalize first word with null');

  // Test case 4: Single character
  expect(getCapitalizeFirstWord('a')).toBe('A');
});
```

# Angular

## Exercice: Is there a problem and improve the code _(5 points)_

```typescript
@Component({
  selector: 'app-users',
  template: `
    <input type="text" [(ngModel)]="query" (ngModelChange)="querySubject.next($event)">
    <div *ngFor="let user of users">
        {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit {

  query = '';
  querySubject = new Subject<string>();

  users: { email: string; }[] = [];

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    concat(
      of(this.query),
      this.querySubject.asObservable()
    ).pipe(
      concatMap(q =>
        timer(0, 60000).pipe(
          this.userService.findUsers(q)
        )
      )
    ).subscribe({
      next: (res) => this.users = res
    });
  }
}
```

### Response

There are a few potential issues:

1. I would prefer to use reactive forms in this case instead of the `[(ngModel)]` with the two way data binding which can lead to complex and error-prone code, and it is not recommended in the latest Angular best practices.
2. Subscribing directly to observable in the component
   this can lead to memory leaks and may not follow best practices. It's better to use the async pipe in the template or unsubscribe from the observable when the component is destroyed (to have that you can use a `destroySubject` and use `takeUntil` on the main observable).

a more cleaner solution would be:

```typescript
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { UserService } from 'path-to-your-user-service';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-users',
  template: `
    <input type="text" [formControl]="queryFormControl">
    <div *ngFor="let user of users$ | async">
      {{ user.email }}
    </div>
  `
})
export class AppUsers implements OnInit {
  queryFormControl = new FormControl(''); // reactive form control
  users$: Observable<{ email: string }[]>; // store uses$ as an observable

  constructor(private userService: UserService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.users$ = this.queryFormControl.valueChanges.pipe(
      debounceTime(300),  // Debounce for 300ms to wait for user to finish typing
      distinctUntilChanged(), // Ensure the search query value has changed
      switchMap(q => this.userService.findUsers(q)) // cancel previous API call if a new one is triggered before the previous completes
    );
  }
}

```

## Exercice: Improve performance _(5 points)_

```typescript
@Component({
  selector: 'app-users',
  template: `
    <!-- remove the function call from the template -->
    <div *ngFor="let user of users">
        {{ user.name }}
    </div>
  `
})
export class AppUsers implements OnInit {

  @Input()
  users: { name: string; }[] = []; // add default value

  ngOnInit(): void {
    // map the user's name
    this.users = this.users.map((user: User): User => ({
      name: this.getCapitalizeFirstWord(user.name)
    }));
  }
  
  getCapitalizeFirstWord(name: string): string {
    return name.split(' ').map(n => n.substring(0, 1).toUpperCase() + n.substring(1).toLowerCase()).join(' ');
  }
}
```

performance improvements:

1. calling the `getCapitalizeFirstWord` function in the template can lead to performance issues so it's better to preprocess the data before it reaches the template.
2. implementing `OnInit` and overriding its `ngOnInit` function in which we can get the `@Input` and map its value
3. if there are any others components which share the same transformation logic of `getCapitalizeFirstWord` it would be better to have a custom `Pipe` and export it for the demanding modules

## Exercice: Forms _(8 points)_

Complete and modify `AppUserForm` class to use Angular Reactive Forms. Add a button to submit.

The form should return data in this format

```typescript
{
  email: string; // mandatory, must be a email
  name: string; // mandatory, max 128 characters
  birthday?: Date; // Not mandatory, must be less than today
  address: { // mandatory
    zip: number; // mandatory
    city: string; // mandatory, must contains only alpha uppercase and lower and space
  };
}
```

```typescript
@Component({
  selector: 'app-user-form',
  template: `
    <form>
        <input type="text" placeholder="email">
        <input type="text" placeholder="name">
        <input type="date" placeholder="birthday">
        <input type="number" placeholder="zip">
        <input type="text" placeholder="city">
    </form>
  `
})
export class AppUserForm {

  @Output()
  event = new EventEmitter<{ email: string; name: string; birthday: Date; address: { zip: number; city: string; };}>;
  
  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  doSubmit(): void {
    this.event.emit(...);
  }
}
```

```typescript
import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  template: `
    <form [formGroup]="userForm" (ngSubmit)="doSubmit()">
      <input type="text" placeholder="email" formControlName="email">
      <div *ngIf="userForm.get('email').hasError('email') && userForm.get('email').touched">
        Invalid email format.
      </div>
      
      <input type="text" placeholder="name" formControlName="name">
      <div *ngIf="userForm.get('name').hasError('maxlength') && userForm.get('name').touched">
        Name must be less than 128 characters.
      </div>
      
      <input type="date" placeholder="birthday" formControlName="birthday">
      <div *ngIf="userForm.get('birthday').hasError('maxDate') && userForm.get('birthday').touched">
        Birthday must be less than today.
      </div>
      <div formGroupName="address">
        <input type="number" placeholder="zip" formControlName="zip">
        <input type="text" placeholder="city" formControlName="city">
      </div>


      <button type="submit" [disabled]="userForm.invalid">Submit</button>
    </form>
  `
})
export class AppUserForm {

  @Output() event = new EventEmitter<{
    email: string;
    name: string;
    birthday?: Date;
    address: {
      zip: number;
      city: string;
    };
  }>();

  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.userForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.maxLength(128)]],
      birthday: ['', [this.maxDateValidator()]],
      address: this.formBuilder.group({
        zip: ['', [Validators.required]],
        city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]]
      }),
    });
  }

  doSubmit(): void {
    if (this.userForm.valid) {
      this.event.emit(this.userForm.value);
    }
  }

  maxDateValidator() {
    return (control) => {
      const currentDate = new Date();
      const selectedDate = new Date(control.value);
      return selectedDate > currentDate ? { maxDate: true } : null;
    };
  }
}

```

# CSS & Bootstrap

## Exercice: Card _(5 points)_

![image](uploads/0388377207d10f8732e1d64623a255b6/image.png)

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <title>Document</title>
    </head>
    <body>
        <div class="card w-75">
            <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                123+
                <span class="visually-hidden">unread messages</span>
            </span>
            <div class="card-body">
            <h5 class="card-title">Exercice</h5>
            <p class="card-text">Redo this card in css and if possible using bootstrap 4 or 5</p>
            <div class="btn-row">
                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                    <input type="radio" class="btn-check" name="btnradio" id="btnradio1" autocomplete="off" checked>
                    <label class="btn btn-outline-dark" for="btnradio1">Got it</label>
                  
                    <input type="radio" class="btn-check" name="btnradio" id="btnradio2" autocomplete="off">
                    <label class="btn btn-outline-dark" for="btnradio2">I don't know</label>
                  </div>
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button">
                      Options
                    </button>
                  </div>
                </div>
            </div>
        </div>
    </body>
    <style>
        body {
            display: flex;
            justify-content: center;
            margin-top: 100px;
        }
        .card-body > .btn-row {
            display: flex;
            justify-content: end;
            gap: 5px;
        }
    </style>
</html>
```

# MongoDb

## Exercice: MongoDb request _(3 points)_

MongoDb collection `users` with schema

``` typescript
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
  }
```

Complete the query, you have a variable that contains a piece of text to search for. Search by exact email, starts with first or last name and only users logged in for 6 months

``` typescript
const searchText = "";
const sixMonthsAgo = new Date();
sixMonthsAgo.setMonth(sixMonthsAgo.getMonth() - 6);

db.collection('users').find({
  $or: [
    { email: searchText },
    { first_name: { $regex: `^${searchText}`, $options: 'i' } }, // Case-insensitive search
    { last_name: { $regex: `^${searchText}`, $options: 'i' } },  // Case-insensitive search
  ],
  last_connection_date: { $gt: sixMonthsAgo },
});
```

What should be added to the collection so that the query is not slow?

we can add indexes to speed up search operations

``` typescript
// Create an index on the 'email' field for exact matching
db.users.createIndex({ email: 1 });

// Create indexes on 'first_name' and 'last_name' for case-insensitive prefix matching
db.users.createIndex({ first_name: 1 }, { collation: { locale: 'en', strength: 2 } });
db.users.createIndex({ last_name: 1 }, { collation: { locale: 'en', strength: 2 } });

// Create an index on 'last_connection_date' for sorting and range queries
db.users.createIndex({ last_connection_date: 1 });
```

## Exercice: MongoDb aggregate _(5 points)_

MongoDb collection `users` with schema

``` typescript
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
  }
```

Complete the aggregation so that it sends user emails by role ({_id: 'role', users: [email,...]})

``` typescript
db.collections('users').aggregate([
  {
    $group: {
      _id: "$roles",
      users: { $push: "$email" }
    }
  },
  {
    $project: {
      _id: 0,
      role: "$_id",
      users: 1
    }
  }
]);

```

## Exercice: MongoDb update _(5 points)_

MongoDb collection `users` with schema

``` typescript
  {
    email: string;
    first_name: string;
    last_name: string;
    roles: string[];
    last_connection_date: Date;
    addresses: {
        zip: number;
        city: string;
    }[]:
  }
```

ps: instead of `await` you can add a `callback` 
Update document `ObjectId("5cd96d3ed5d3e20029627d4a")`, modify only `last_connection_date` with current date

``` typescript
await db.collections('users').updateOne(
  { _id: "5cd96d3ed5d3e20029627d4a" },
  { $set: { last_connection_date: new Date() } }
);
```

Update document `ObjectId("5cd96d3ed5d3e20029627d4a")`, add a role `admin`

``` typescript
await db.collections('users').updateOne(
  { _id: "5cd96d3ed5d3e20029627d4a" },
  { $set: { $push: { roles: 'admin' } } }
);
```

Update document `ObjectId("5cd96d3ed5d3e20029627d4a")`, modify addresses with zip `75001` and replace city with `Paris 1`

``` typescript
await db.collections('users').updateOne(
  { _id: "5cd96d3ed5d3e20029627d4a", "addresses.zip": 75001 },
  { $set: { "addresses.$.city": "Paris 1" } }
);
```
